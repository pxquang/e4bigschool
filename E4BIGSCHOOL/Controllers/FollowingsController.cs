﻿using E4BIGSCHOOL.DTOs;
using E4BIGSCHOOL.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace E4BIGSCHOOL.Controllers
{
    public class FollowingsController : ApiController
    {

        private readonly ApplicationDbContext _context;

        public FollowingsController()
        {
            _context = new ApplicationDbContext();
        }

        [HttpPost]
        public IHttpActionResult Follow(FollowingDto followingDto)
        {
            var userId = User.Identity.GetUserId();
            if (_context.Followings.Any(a => a.FollowerId == userId && a.FolloweeId == followingDto.FoloweeId))
                return BadRequest("Following already exists!");
            var following = new Following
            {
                FollowerId = userId,
                FolloweeId = followingDto.FoloweeId
            };

            _context.Followings.Add(following);
            _context.SaveChanges();
            return Ok();
        }






    }
}
