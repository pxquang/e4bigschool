﻿using E4BIGSCHOOL.Models;
using E4BIGSCHOOL.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E4BIGSCHOOL.Controllers
{
    public class CoursesController : Controller
    {
        private readonly ApplicationDbContext _context; 
        
        public CoursesController() { 
            _context = new ApplicationDbContext();
        }
            
        // GET: Courses
       
        [Authorize]
        public ActionResult Create()
        {
            var viewModel = new CourseViewModel()
            {
                Categories = _context.Categories.ToList()
            };
            return View(viewModel);
        }


        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CourseViewModel model)
        {
            if (!ModelState.IsValid)
            {
                model.Categories = _context.Categories.ToList();
                return View("Create", model);
            }

            var course = new Course
            {
                LecturerId = User.Identity.GetUserId(),
                Date = model.GetDateTime(),
                CategoryId = model.Category,
                Place = model.Place
            };

            _context.Courses.Add(course);
            _context.SaveChanges();

            return RedirectToAction("Index", "Home");

        }

        [Authorize]
        public ActionResult Attending()
        {
            var userId = User.Identity.GetUserId();

            var courses = _context.Attendances
                .Where(a => a.AttendeeId == userId)
                .Select(a => a.Course)
                .Include(a => a.Lecturer)
                .Include(l => l.Category)
                .ToList();

            var viewModel = new CourseViewModel
            {
                UpcommingCourses = courses,
                ShowAction = User.Identity.IsAuthenticated
            };
            return View (viewModel);
        }
    }
}