﻿using E4BIGSCHOOL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using E4BIGSCHOOL.ViewModels;

namespace E4BIGSCHOOL.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext _context;

        public HomeController() { 
            _context = new ApplicationDbContext();
        }
        public ActionResult Index()
        {
            var upcomingCourses = _context.Courses
                .Include(c => c.Lecturer)
                .Include(c => c.Category)
                .Where(c => c.Date > DateTime.Now);

            var viewModel = new CourseViewModel
            {
                UpcommingCourses = upcomingCourses,
                ShowAction = User.Identity.IsAuthenticated
            };
            return View(viewModel);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}