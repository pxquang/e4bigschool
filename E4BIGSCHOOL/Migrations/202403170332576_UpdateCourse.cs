﻿namespace E4BIGSCHOOL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateCourse : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Courses", "LecturerId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.Courses", "LecturerId");
            AddForeignKey("dbo.Courses", "LecturerId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            DropColumn("dbo.Courses", "Lecture");
            DropColumn("dbo.Courses", "LectureId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Courses", "LectureId", c => c.String(nullable: false));
            AddColumn("dbo.Courses", "Lecture", c => c.String(nullable: false));
            DropForeignKey("dbo.Courses", "LecturerId", "dbo.AspNetUsers");
            DropIndex("dbo.Courses", new[] { "LecturerId" });
            DropColumn("dbo.Courses", "LecturerId");
        }
    }
}
