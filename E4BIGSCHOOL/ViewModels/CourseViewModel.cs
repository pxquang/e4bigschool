﻿using E4BIGSCHOOL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;

namespace E4BIGSCHOOL.ViewModels
{
    public class CourseViewModel
    {
        public IEnumerable<Course> UpcommingCourses { get; set; }
        public bool ShowAction {  get; set; }


        [Required]
        public string Place {  get; set; }
        [Required]
        [FutureDate]
        public string Date  { get; set; }
        [Required]
        [ValidTime]
        public string Time { get; set; }
        [Required]
        public byte Category { get; set; }
        public IEnumerable<Category> Categories { get; set; }   
        public DateTime GetDateTime() {
            //  return DateTime.Parse(string.Format("{0} {1}",Date, Time)); 
            
                //return DateTime.Parse(string.Format("{0} {1}", Date, Time));
                string dateTimeString = $"{Date} {Time}";

                // Parse the date and time separately
                DateTime parsedDate;
                if (!DateTime.TryParseExact(Date, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out parsedDate))
                {
                    throw new FormatException("Invalid date format.");
                }

                TimeSpan parsedTime;
                if (!TimeSpan.TryParseExact(Time, "hh\\:mm", CultureInfo.InvariantCulture, out parsedTime))
                {
                    throw new FormatException("Invalid time format.");
                }

                // Combine parsed date and time
                DateTime result = parsedDate.Date.Add(parsedTime);

                return result;
            

        }
    }
}